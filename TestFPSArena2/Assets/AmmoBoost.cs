﻿using UnityEngine;
using System.Collections;

public class AmmoBoost : MonoBehaviour
{
	private const float _boostRespawnTime = 30f; // spawn ammoBoosts every 30 seconds

	private static Transform _slot;
	private static Transform[] _ammoBoost;
	private static readonly int _boostLimit = 5;

	private static bool _locker = false;

	void Update()
	{
		transform.Rotate(2f, 0f, 2f);
	}

	public static void Initialize(Transform slot)
	{
		_slot = slot;
	}

	public static void Spawn(float timer)
	{
		if (Mathf.RoundToInt(timer % _boostRespawnTime) == 0f)
		{
			if (!_locker)
			{
				_locker = true;
				AmmoBoostSpawn();
			}
		}
		else
		{
			if (_locker)
			{
				_locker = false;
			}
		}
	}

	public static void AmmoBoostSpawn()
	{
		if (_ammoBoost == null || _ammoBoost.Length < _boostLimit)
		{
			_ammoBoost = new Transform[_boostLimit];
		}
		
		for (int i = 0; i < _ammoBoost.Length; i++)
		{
			if (_ammoBoost[i] == null)
			{
				Vector3 ammoSpawnPoint = Vector3.zero;
				RaycastHit hitInfo;
				
				bool spawnAllowed = true;
				do
				{
					ammoSpawnPoint = new Vector3(Random.Range(1f, 79f), 0.375f, Random.Range(1f, 79f));
					Vector3 raycastVector = new Vector3(ammoSpawnPoint.x, 10f, ammoSpawnPoint.z);
					
					Physics.SphereCast(raycastVector, 0.5f, Vector3.down, out hitInfo, 10f);
					
					if (hitInfo.transform.tag != "Obstacle" && Player.SpawnAllowed(ammoSpawnPoint))
					{
						spawnAllowed = false;
					}
				}
				while (spawnAllowed);
				
				_ammoBoost[i] = Instantiate(Resources.Load<Transform>("Prefabs/ammoBoost"), ammoSpawnPoint, Quaternion.identity) as Transform;
				_ammoBoost[i].parent = _slot;
			}
		}
	}
}
