﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SimpleJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
	private Image backgroundImage;
	private Image joystickImage;

	private Vector3 inputVector;

	private bool _active = false;

	private void Start()
	{
		backgroundImage = GetComponent<Image>();
		joystickImage = transform.GetChild(0).GetComponent<Image>();
	}

	public virtual void OnDrag(PointerEventData data)
	{
		Vector2 clickPosition;

		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
			backgroundImage.rectTransform,
			data.position,
			data.pressEventCamera,
			out clickPosition))
		{
			clickPosition.x = (clickPosition.x / backgroundImage.rectTransform.sizeDelta.x);
			clickPosition.y = (clickPosition.y / backgroundImage.rectTransform.sizeDelta.y);

			inputVector = new Vector3(clickPosition.x * 2 + 1, 0, clickPosition.y * 2 - 1);
			inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized: inputVector;

			joystickImage.rectTransform.anchoredPosition = new Vector3(
				inputVector.x * (backgroundImage.rectTransform.sizeDelta.x / 3),
				inputVector.z * (backgroundImage.rectTransform.sizeDelta.y / 3),
				0);
		}
	}

	public virtual void OnPointerDown(PointerEventData data)
	{
		_active = true;
		OnDrag(data);
	}

	public virtual void OnPointerUp(PointerEventData data)
	{
		inputVector = Vector3.zero;
		joystickImage.rectTransform.anchoredPosition = inputVector;
		_active = false;
	}

	public float Horizontal()
	{
		if (inputVector.x != 0)
		{
			return inputVector.x;
		}
		else
		{
			return Input.GetAxis("Horizontal");
		}
	}

	public float Vertical()
	{
		if (inputVector.z != 0)
		{
			return inputVector.z;
		}
		else
		{
			return Input.GetAxis("Vertical");
		}
	}

	public bool Active()
	{
		return _active;
	}
}
