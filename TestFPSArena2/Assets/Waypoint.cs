﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour
{
	private static Transform _slot;

	public static Transform[] Point;

	public int index = 0;

	void Start()
	{
		transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
	}

	public static void Initialize(Transform slot)
	{
		_slot = slot;

		Point = new Transform[_slot.childCount];
		
		for (int i = 0; i < Waypoint.Point.Length; i++)
		{
			Point[i] = _slot.GetChild(i);
			Point[i].GetComponent<Waypoint>().index = i;
		}
	}
}
