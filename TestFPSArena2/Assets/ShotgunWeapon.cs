﻿using UnityEngine;
using System.Collections;

public class ShotgunWeapon : Weapon
{	
	protected override void Awake()
	{
		_animation = gameObject.GetComponent<Animation>();

		_idle = _animation.GetClip("ShotgunIdle");
		_fire = _animation.GetClip("ShotgunFire");

		base.Awake();

		Damage = 50;
		GunAmmo = 20;
	}
	
	public override void Fire()
	{
		if (_canShoot)
		{
			_canShoot = false;
			Shoot();
		}
	}
	
	protected override IEnumerator CoroutineShoot()
	{
		yield return new WaitForSeconds(1.5f);
		
		_canShoot = true;
		
		yield break;
	}
}
