﻿using UnityEngine;
using System.Collections;

public class BotSpawner : MonoBehaviour
{
	private static Transform _enemiesSlot;

	private static Transform _slot;
	private static Transform[] _spawners;

	private static readonly int _botLimit = 5;
	private static Transform[] _bot;

	public float SpawnRadius = 30f;

	public float CoolingTime = 5f;
	public bool CoolsDown = false;

	void Start()
	{
		GetComponent<MeshRenderer>().enabled = false;
	}

	public void SpawnBot()
	{
		if (!CoolsDown)
		{
			ForcedSpawnBot();
		}
	}

	public void ForcedSpawnBot()
	{
		for (int i = 0; i < _bot.Length; i++)
		{
			if (_bot[i] == null || !_bot[i].gameObject.activeSelf)
			{
				_bot[i] = Instantiate(Resources.Load<Transform>("Prefabs/MAXbot"), transform.position, Quaternion.identity) as Transform;
				_bot[i].parent = _enemiesSlot;
				break;
			}
			
			if (i == _bot.Length - 1)
			{
				return;
			}
		}
		
		CoolsDown = true;
		StartCoroutine(Cooling());
	}

	private IEnumerator Cooling()
	{
		yield return new WaitForSeconds(CoolingTime);
		CoolsDown = false;
		yield break;
	}

	public static void Spawn()
	{
		ArrayList avalibleSpawners = new ArrayList();

		for (int i = 0; i < _spawners.Length; i++)
		{
			if (Vector3.Distance(Main.PlayerStartPosition, _spawners[i].transform.position) > _spawners[i].GetComponent<BotSpawner>().SpawnRadius &&
			    !_spawners[i].GetComponent<BotSpawner>().CoolsDown)
			{
				avalibleSpawners.Add(_spawners[i]);
			}
		}

		if (avalibleSpawners.Count > 0)
		{
			(avalibleSpawners[Random.Range(0, avalibleSpawners.Count - 1)] as Transform).GetComponent<BotSpawner>().SpawnBot();
		}
		else
		{
			_spawners[Random.Range(0, _spawners.Length - 1)].GetComponent<BotSpawner>().SpawnBot();
		}
	}

	public static void Respawn(GameObject obj)
	{
		Destroy(obj);
		Spawn();
	}

	public static void Initialize(Transform slot, Transform enemiesSlot)
	{
		_slot = slot;
		_enemiesSlot = enemiesSlot;

		_bot = new Transform[_botLimit];

		_spawners = new Transform[_slot.childCount];
		
		for (int i = 0; i < _spawners.Length; i++)
		{
			_spawners[i] = _slot.GetChild(i);
		}

		for (int i = 0; i < _botLimit; i++)
		{
			Spawn();
		}
	}
}
