﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum CurrentWeapon
{
	Nothing,
	Automat,
	Shotgun
};

public class Player : MonoBehaviour
{
	private static readonly float _spawnBlockRadius = 20f;
	private static Player _instance;

	public static Vector3 CurrentPosition
	{
		get
		{
			return _instance.transform.position;
		}
	}

	private Transform _body;
	private Transform _head;
	private Weapon _gun;
	private CharacterController _character;

	private float _verticalRotation;
	private float _forwardBackwardSpeed;
	private float _leftRightSpeed;
	private Vector3 _movementVector;

	private bool _tapped = false;
	private bool _swiped = false;

	private CurrentWeapon _gunType = CurrentWeapon.Nothing;

	public float UpDownBorder = 45f;
	public float HorizontalSens = 240f;
	public float VerticalSens = 120f;
	public float FBVelocity = 6f;
	public float LRVelocity = 6f;

	public SimpleJoystick Joystick;
	public Transform AutomatPlaceholder;
	public Transform ShotgunPlaceholder;
	public Text AmmoPlaceholder;
	
	void Start()
	{
		transform.position = Main.PlayerStartPosition;

		_instance = this;

		_body = transform;
		_head = Camera.main.transform;
		_character = GetComponent<CharacterController>();

		if (AutomatPlaceholder != null)
		{
			AutomatPlaceholder.gameObject.SetActive(false);
		}

		if (ShotgunPlaceholder != null)
		{
			ShotgunPlaceholder.gameObject.SetActive(false);
		}
		
		SetNewGun(CurrentWeapon.Automat);
	}

	void Update()
	{
		TapOrSwipe();
	}
	
	void LateUpdate()
	{
		if (!Joystick.Active() && _tapped)
		{
			_gun.Fire();
		}

		#if UNITY_EDITOR
		if (!Joystick.Active() && Input.GetMouseButton(0))
		{
			PlayerRotation();
		}
		#endif
		#if UNITY_ANDROID
		if (!Joystick.Active() && _swiped)
		{
			PlayerRotation();
		}
		#endif

		PlayerMovement();
	}

	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		Rigidbody body = hit.collider.attachedRigidbody;

		if (body == null)
		{
			return;
		}

		switch (body.gameObject.tag)
		{
		case "Boost":

			_gun.GunAmmo += 20;
			Destroy(body.gameObject);
			break;

		case "Automat":

			SetNewGun(CurrentWeapon.Automat);
			break;

		case "Shotgun":
			
			SetNewGun(CurrentWeapon.Shotgun);
			break;

		default:

			break;
		}
	}

	public void TapOrSwipe()
	{
		_tapped = false;
		
		if (Input.touchCount > 0)
		{
			switch(Input.GetTouch(0).phase)
			{
			case TouchPhase.Began:
				
				_swiped = false;
				break;
				
			case TouchPhase.Moved:
				
				_swiped = true;
				break;
				
			case TouchPhase.Ended:
				
				if (!_swiped)
				{
					_tapped = true;
				}
				break;
			}
		}

		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0))
		{
			_tapped = true;
		}
		#endif
	}

	public void PlayerRotation()
	{
		float _horizontalRotation = Input.GetAxis("Mouse X") * HorizontalSens * 0.02f;
		_body.Rotate(0f, _horizontalRotation, 0f);

		_verticalRotation -= Input.GetAxis("Mouse Y") * VerticalSens * 0.02f;
		_verticalRotation = Mathf.Clamp(_verticalRotation, -UpDownBorder, UpDownBorder);
		_head.localRotation = Quaternion.Euler(_verticalRotation, 0f, 0f);
	}
	
	public void PlayerMovement()
	{
		_forwardBackwardSpeed = Joystick.Vertical() * FBVelocity;
		_leftRightSpeed = Joystick.Horizontal() * LRVelocity;
		
		_movementVector = new Vector3(_leftRightSpeed, 0f, _forwardBackwardSpeed);
		_movementVector = _body.rotation * _movementVector;
		
		_character.SimpleMove(_movementVector);
	}
	
	public void SetNewGun(CurrentWeapon type)
	{
		if (_gunType == type)
		{
			return;
		}

		if (_gun != null)
		{
			Destroy(_gun.gameObject);
		}
		
		switch (type)
		{
		case CurrentWeapon.Automat:

			_gun = Instantiate(Resources.Load<Weapon>("guns/Prefabs/Automatic Rifle Standard"));

			if (_gun != null && AutomatPlaceholder != null)
			{
				_gun.transform.parent = _head;
				_gun.transform.position = AutomatPlaceholder.position;
				_gun.transform.rotation = AutomatPlaceholder.rotation;
				_gun.transform.localScale = AutomatPlaceholder.localScale;
				_gun.gameObject.layer = 8;

				if (AmmoPlaceholder != null)
				{
					_gun.AmmoPlaceholder = AmmoPlaceholder;
					_gun.AmmoPlaceholder.text = _gun.GunAmmo.ToString();
				}
			}

			_gunType = CurrentWeapon.Automat;

			break;
			
		case CurrentWeapon.Shotgun:

			_gun = Instantiate(Resources.Load<Weapon>("guns/Prefabs/Frontloader Standard"));

			if (_gun != null && ShotgunPlaceholder != null)
			{
				_gun.transform.parent = _head;
				_gun.transform.position = ShotgunPlaceholder.position;
				_gun.transform.rotation = ShotgunPlaceholder.rotation;
				_gun.transform.localScale = ShotgunPlaceholder.localScale;
				_gun.gameObject.layer = 8;
				
				if (AmmoPlaceholder != null)
				{
					_gun.AmmoPlaceholder = AmmoPlaceholder;
					_gun.AmmoPlaceholder.text = _gun.GunAmmo.ToString();
				}
			}

			_gunType = CurrentWeapon.Shotgun;

			break;
		}
	}

	public static bool SpawnAllowed(Vector3 point)
	{
		return Vector3.Distance(point, _instance.transform.position) > Player._spawnBlockRadius;
	}
}
