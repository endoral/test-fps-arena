﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Weapon : MonoBehaviour
{
	protected Animation _animation;
	
	protected AnimationClip _idle;
	protected AnimationClip _fire;
	
	protected float _range = 80f;
	protected bool _canShoot = true;

	private int _gunAmmo;

	public float Damage;

	public Text AmmoPlaceholder;

	public int GunAmmo
	{
		get
		{
			return _gunAmmo;
		}
	
		set
		{
			if (value < 0)
			{
				return;
			}
			else
			{
				if (value > 0 && _gunAmmo == 0)
				{
					_canShoot = true;
				}
			}
	
			_gunAmmo = value;
	
			if (AmmoPlaceholder != null)
			{
				AmmoPlaceholder.text = value.ToString();
			}
		}
	}
	
	protected virtual void Awake()
	{
		_animation.wrapMode = WrapMode.Loop;
		_animation[_fire.name].wrapMode = WrapMode.Clamp;
		_animation[_idle.name].layer = -1;
		_animation.Stop();
	}

	void Update()
	{
		_animation.CrossFade(_idle.name);
	}
	
	public abstract void Fire();

	protected virtual void Shoot()
	{
		GunAmmo--;
		
		_animation.Stop();
		_animation.CrossFade(_fire.name, 0.1f, PlayMode.StopAll);
		
		Vector3 DirectionRay = Camera.main.transform.TransformDirection(Vector3.forward);
		RaycastHit hit;

		if(Physics.Raycast(Camera.main.transform.position, DirectionRay, out hit, _range))
		{
			if(hit.collider)
			{
				#if UNITY_EDITOR
				Debug.DrawLine(Camera.main.transform.position, hit.point, Color.red, 20, true);
				Debug.Log("Shoot in |" + hit.collider.name + "| at a distance |" + hit.distance + "|");
				#endif
				
				if (hit.collider.tag == "Enemy")
				{
					hit.collider.GetComponent<EnemyAI>().SetDamage(Damage);
				}
			}
		}
		
		if (GunAmmo > 0)
		{
			StartCoroutine(CoroutineShoot());
		}
	}
	
	protected abstract IEnumerator CoroutineShoot();
}
