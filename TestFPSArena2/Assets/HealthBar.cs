﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour
{
	public Image Bar;

	void Update ()
	{
		transform.LookAt(Camera.main.transform);
	}

	public void SetHealth(float health)
	{
		float percent = Mathf.Clamp(health / 100f, 0f, 1f);
		Vector3 barScale = new Vector3(percent, Bar.transform.localScale.y, Bar.transform.localScale.z);
		Bar.transform.localScale = barScale;
	}
}
