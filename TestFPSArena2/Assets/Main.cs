﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{
	public static Vector3 PlayerStartPosition = new Vector3(5f, 0f, 5f);

	private const float _timerLimit = 600f; // reset every 10 minutes

	public Transform EnemiesSlot;
	public Transform InteractiveObjectsSlot;
	public Transform WaypointsSlot;
	public Transform SpawnersSlot;

	public float timer = 0f;

	void Start()
	{
		AmmoBoost.Initialize(InteractiveObjectsSlot);
		Waypoint.Initialize(WaypointsSlot);
		BotSpawner.Initialize(SpawnersSlot, EnemiesSlot);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}

		TimerTick();
		AmmoBoost.Spawn(timer);
	}

	private void TimerTick()
	{
		if (timer >= _timerLimit)
		{
			timer = 0f;
		}
		
		timer += Time.deltaTime;
	}
}
