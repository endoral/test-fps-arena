﻿using UnityEngine; 
using System.Collections; 

// - В искусственном интеллекте я, мягко говоря, не силен, так что не судите строго...

public enum BotState
{
	DoNothing,
	Walk,
	Run,
	Runaway,
	Dead
}

public class EnemyAI : MonoBehaviour
{ 
	private CharacterController _character;
	private NavMeshAgent _agent;
	private Animation _animation;

	private AnimationClip _idle;
	private AnimationClip _walk;
	private AnimationClip _run;
	private AnimationClip _death;

	private BotState _state = BotState.DoNothing;

	private bool runawayTurn = false;
	private float _safeRadius = 30f;

	private float _walkAcceleration = 8f;
	private float _walkSpeed = 2.2f;
	private float _walkAngularSpeed = 120f;

	private float _runAcceletarion = 18f;
	private float _runSpeed = 7f;
	private float _runAngularSpeed = 360f;

	private float _HP = 100f;

	private Waypoint _targetPoint;
	private int _currentWaypointIndex = -1;

	public HealthBar HPBar;

	private bool _inSafe
	{
		get
		{
			return Vector3.Distance(transform.position, Player.CurrentPosition) > _safeRadius;
		}
	}

	private bool _playerInFront
	{
		get
		{
			if (!_inSafe && transform.InverseTransformPoint(Player.CurrentPosition).z > 0)
			{
				return true;
			}

			return false;
		}
	}

	public float HP
	{
		get
		{
			return _HP;
		}

		set
		{
			if (value < 0f)
			{
				_HP = 0;
			}
			else
			{
				if (value > 100f)
				{
					_HP = 100f;
				}
				else
				{
					_HP = value;
				}
			}

			HPBar.SetHealth(HP);
		}
	}

	public BotState WalkOrRun
	{
		get
		{
			return Random.Range(0, 2) == 0 ? BotState.Walk : BotState.Run;
		}
	}

	void Start()
	{ 
		_animation = GetComponent<Animation>();

		_idle = _animation.GetClip("idle");
		_walk = _animation.GetClip("walk");
		_run = _animation.GetClip("run");
		_death = _animation.GetClip("death");

		_animation.wrapMode = WrapMode.Loop;
		_animation[_idle.name].wrapMode = WrapMode.Loop;
		_animation[_walk.name].wrapMode = WrapMode.Loop;
		_animation[_run.name].wrapMode = WrapMode.Loop;
		_animation[_death.name].wrapMode = WrapMode.Clamp;
		_animation.Stop();

		_character = GetComponent<CharacterController>();
		_agent = GetComponent<NavMeshAgent>();

		HPBar.SetHealth(HP);

		SetBotState(WalkOrRun);
	}
	
	void Update()
	{ 
		_character.SimpleMove(Vector3.zero);

		RunawaySafeTarget();
	}

	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		Rigidbody body = hit.collider.attachedRigidbody;
		
		if (body == null)
		{
			return;
		}
		
		switch (body.tag)
		{
		case "Waypoint":

			if (body.gameObject == _targetPoint.gameObject)
			{
				if (_state == BotState.Runaway && !_inSafe)
				{
					SetNewSafeTarget();
					return;
				}

				SetBotState(WalkOrRun);
			}

			break;
			
		default:

			break;
		}
	}

	public void SetNewTarget()
	{
		do
		{
			_targetPoint = Waypoint.Point[Random.Range(0, 17)].GetComponent<Waypoint>();
		}
		while (_targetPoint.index == _currentWaypointIndex);

		_currentWaypointIndex = _targetPoint.index;

		_agent.SetDestination(_targetPoint.transform.position);
	}

	public void SetNewSafeTarget()
	{
		float currentDistance = 0f;
		float maxDistance = 0f;
		int currentPoint = 0;
		float maxSafeDistance = 0f;
		int currentSafePoint = -1;

		for (int i = 0; i < Waypoint.Point.Length; i++)
		{
			currentDistance = Vector3.Distance(transform.position, Waypoint.Point[i].position);

			if (_playerInFront)
			{
				if (currentDistance > maxSafeDistance && transform.InverseTransformPoint(Waypoint.Point[i].position).z <= 0)
				{
					maxSafeDistance = currentDistance;
					currentSafePoint = i;
				}
			}
			else
			{
				if (currentDistance > maxSafeDistance && transform.InverseTransformPoint(Waypoint.Point[i].position).z > 0)
				{
					maxSafeDistance = currentDistance;
					currentSafePoint = i;
				}
			}

			if (currentDistance > maxDistance)
			{
				maxDistance = currentDistance;
				currentPoint = i;
			}
		}

		if (currentSafePoint >= 0)
		{
			_targetPoint = Waypoint.Point[currentSafePoint].GetComponent<Waypoint>();
		}
		else
		{
			_targetPoint = Waypoint.Point[currentPoint].GetComponent<Waypoint>();
		}

		_currentWaypointIndex = _targetPoint.index;

		_agent.SetDestination(_targetPoint.transform.position);
	}

	public void SetBotState(BotState state)
	{
		if (_state != BotState.Dead)
		{
			switch (state)
			{
			case BotState.DoNothing:

				_agent.Stop();
				_animation.CrossFade(_idle.name);
				break;
			
			case BotState.Walk:

				SetNewTarget();
				_agent.acceleration = _walkAcceleration;
				_agent.speed = _walkSpeed;
				_agent.angularSpeed = _walkAngularSpeed;
				_animation.CrossFade(_walk.name);

				break;
			
			case BotState.Run:

				SetNewTarget();
				_agent.acceleration = _runAcceletarion;
				_agent.speed = _runSpeed;
				_agent.angularSpeed = _runAngularSpeed;
				_animation.CrossFade(_run.name);

				break;
			
			case BotState.Runaway:

				SetNewSafeTarget();
				_agent.acceleration = _runAcceletarion;
				_agent.speed = _runSpeed;
				_agent.angularSpeed = _runAngularSpeed;
				_animation.CrossFade(_run.name);

				break;
			
			case BotState.Dead:

				_agent.Stop();
				_animation.CrossFade(_death.name);
				StartCoroutine(Death());
				break;
			}

			_state = state;
		}
	}

	public void SetDamage(float value)
	{
		HP -= value;

		if (HP == 0)
		{
			SetBotState(BotState.Dead);
		}
		else
		{
			if (_state != BotState.Runaway)
			{
				SetBotState(BotState.Runaway);
			}
		}
	}

	private void RunawaySafeTarget()
	{
		if (_state == BotState.Runaway && _playerInFront && !runawayTurn)
		{
			runawayTurn = true;
			SetNewSafeTarget();
			StartCoroutine(RunawayTurn());
		}
	}

	private IEnumerator RunawayTurn()
	{
		yield return new WaitForSeconds(1.5f);
		runawayTurn = false;
		yield break;
	}

	private IEnumerator Death()
	{
		yield return new WaitForSeconds(1.5f);
		gameObject.SetActive(false);
		BotSpawner.Respawn(gameObject);
		yield break;
	}
}
