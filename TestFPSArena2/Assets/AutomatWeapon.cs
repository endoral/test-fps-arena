﻿using UnityEngine;
using System.Collections;

public enum FireMode
{
	SingleShoot,
	AutoBurst
};

public class AutomatWeapon : Weapon
{
	private int _bulletsToGo;
	
	public FireMode fireMode = FireMode.SingleShoot;
	
	protected override void Awake()
	{
		_animation = gameObject.GetComponent<Animation>();

		_idle = _animation.GetClip("AutomatIdle");
		_fire = _animation.GetClip("AutomatFire");

		base.Awake();

		Damage = 20;
		GunAmmo = 60;
	}
	
	public override void Fire()
	{
		if (_canShoot)
		{
			_canShoot = false;
			
			if (fireMode == FireMode.AutoBurst)
			{
				_bulletsToGo = 2;
			}
			
			Shoot();
		}
	}
	
	protected override IEnumerator CoroutineShoot()
	{
		switch (fireMode)
		{
		case FireMode.SingleShoot:
			
			yield return new WaitForSeconds(0.1f);
			
			_canShoot = true;

			yield break;
			
		case FireMode.AutoBurst:
			
			yield return new WaitForSeconds(0.1f);
			
			if (_bulletsToGo > 0)
			{
				_bulletsToGo--;
				Shoot();
			}
			else
			{
				yield return new WaitForSeconds(0.1f);
				
				_canShoot = true;
				
				yield break;
			}
			
			break;
		}
	}
}
